var Card = (function(){
    //const
    var CARDS_NAME = ['two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'jack', 'queen', 'king', 'ace'];
    //Constructor
    function Card(suitType, value) {
        this.suitType = suitType;
        this.value = value;
        this.name = CARDS_NAME[value - 2];
        this.image = getSrcImage(this.name, suitType);
    }

    //private methods
    function getSrcImage(name, suitType) {
        return 'images/' + suitType + '/' + name + '_of_' + suitType + '.png';
    }

    //public methods
    Card.prototype.toString = function () {
        return this.name + " of " + this.suitType;
    };

    return Card;
}());




