(function () {
    if (Modernizr.canvas) {
        var deckOfCards = new DeckOfCards();
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        var CARD_W = 100;
        var CARD_H = 150;
        var X_POS_START = canvas.width - CARD_W;
        var X_POS_END = canvas.width / 2 - CARD_W / 2;
        var Y_POS = canvas.height - CARD_H;
        var _deckImage, card;
        drawDeck();

        $('#getCard').on('click', function () {
            getRandomCard();
        });

    } else {
        alert("sorry, your browser doesn't support canvas element");
    }

    function drawDeck() {
        if (context) {
            var deckImage = new Image();
            deckImage.src = deckOfCards.image;
            deckImage.onload = function () {
                context.drawImage(deckImage, X_POS_START, Y_POS, CARD_W, CARD_H);
                _deckImage = deckImage;
            };
        }
    }
    function drawCard(xPos, yPos, image) {
        if (context) {
            var cardImage = new Image();
            cardImage.src = image;
            cardImage.onload = function () {
                context.clearRect(0, 0, canvas.width, canvas.height);
                context.drawImage(_deckImage, X_POS_START, Y_POS, CARD_W, CARD_H);
                context.drawImage(cardImage, xPos, yPos, CARD_W, CARD_H);
            };
        }
    }

    function getRandomCard() {
        if (context) {

            if (card) {//move back old card
                card = deckOfCards.getRandomCard();
                moveCard(X_POS_END, X_POS_START, 'out', card);
            } else {
                card = deckOfCards.getRandomCard();
                moveCard(X_POS_START, X_POS_END, 'in', card);
            }
        }
    }

    function moveCard(xPosStart, xPosEnd, command, card) {
        var dX = 2;
        var cardImage = new Image();
        cardImage.src = 'images/card0.png';
        cardImage.onload = function () {
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(_deckImage, X_POS_START, Y_POS, CARD_W, CARD_H);
            context.drawImage(cardImage, xPosStart, Y_POS, CARD_W, CARD_H);
            if (command === 'in') {
                xPosStart -= dX;
                if (xPosStart > xPosEnd) {
                    moveCard(xPosStart, xPosEnd, command, card);
                } else {
                    drawCard(X_POS_END, Y_POS, card.image);
                }
            } else if (command === 'out') {
                xPosStart += dX;
                if (xPosStart < xPosEnd) {
                    moveCard(xPosStart, xPosEnd, command, card);
                } else { // after out move new card in
                    moveCard(X_POS_START, X_POS_END, 'in', card);
                }
            }


        };

    }



}());
