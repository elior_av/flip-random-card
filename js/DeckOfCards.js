var DeckOfCards = (function(){
    //const
    var SUIT_TYPES = {
        Hearts: 'hearts',
        Diamonds: 'diamonds',
        Clubs: 'clubs',
        Spades: 'spades'
    };

    //Constructor
    function DeckOfCards(image) {
        this.cards = createDeck();
        this.image = image || 'images/deckOfCards.png';
    }

    //private methods
    function createDeck() {
        var res = [];
        for (var suit in SUIT_TYPES) {
            for (var i=2; i<=14; i++){
                res.push(new Card(SUIT_TYPES[suit],i));
            }
        }
        return res;
    }

    //public methods
    DeckOfCards.prototype.toString = function () {
        var res = "";
        for(var i = 0; i<this.cards.length; i++){
            res += this.cards[i].toString() + '\n';
        }
        return res;
    };

    DeckOfCards.prototype.getRandomCard = function () {
        return this.cards[Math.floor(Math.random() * this.cards.length)];
    };

    return DeckOfCards;
}());




